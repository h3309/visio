import email
import imaplib
from bs4 import BeautifulSoup

mail = imaplib.IMAP4_SSL("imap.gmail.com")
mail.login('developer.mus@gmail.com', 'Musdev99')

mail.select("inbox")

result, data = mail.uid('search', None, "ALL")

inbox_item_list = data[0].split()

newest = inbox_item_list[-1]
oldest = inbox_item_list[0]

result2, email_data = mail.uid('fetch', inbox_item_list, '(RFC822)')
raw_email = email_data[0][1].decode("utf-8")
email_message = email.message_from_string(raw_email)
to_ = email_message['To']
from_ = email_message['From']
subject_ = email_message['Subject']
counter =1
for part in email_message.walk():
    if part.get_content_maintype() == "multipart":
        continue
    filename = part.get_filename()
    if not filename:
        ext = 'html'
        filename = 'msg-part-%08d%s' %(counter, ext)
    counter += 1

content_type = part.get_content_type()
print(subject_)
if "plain" in content_type:
    print(part.get_payload())
