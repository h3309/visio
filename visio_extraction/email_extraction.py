#!/usr/bin/env python
# coding: utf-8

# In[240]:


import email
import imaplib
from bs4 import BeautifulSoup


# # Access to mailbox

# In[241]:


# login into email
mail = imaplib.IMAP4_SSL("imap.gmail.com")
mail.login('developer.mus@gmail.com', 'Musdev99')
mail.select("inbox")


# # Read the email

# In[242]:



result, data = mail.uid('search', None, "UNSEEN")

inbox_item_list = data[0].split()
newest =  inbox_item_list[-1]


# In[243]:


result2, email_data = mail.uid('fetch', newest, '(RFC822)')
raw_email = email_data[0][1].decode("utf-8")
email_message = email.message_from_string(raw_email)
to_ = email_message['To']
from_ = email_message['From']
subject_ = email_message['Subject']
counter =1


# In[244]:


for part in email_message.walk():
    if part.get_content_maintype() == "multipart":
        continue
    filename = part.get_filename()
    if not filename:
        ext = 'html'
        filename = 'msg-part-%08d%s' %(counter, ext)
    counter += 1


# In[245]:



content_type = part.get_content_type()
print(subject_)
if "plain" in content_type:
    
    print(part.get_payload())
    
    


# In[246]:


part.get_payload()


# # Extract the data

# In[247]:


desc = part.get_payload().split("\n")
print(desc)


# # Regex
# 

# In[248]:


name = desc[0].replace('Product:','')
name = name.replace('\r','')
description = desc[1].replace('Description:','')
description = description.replace('\r','')
qty = desc[2].replace('Quantity:','')
print(name)
print(description)
print(qty)


# # CONVERT DATA INTO CSV
# 

# In[249]:


import pandas as pd # to scrub data as DataFrame creating csv file
import time
from datetime import datetime # get current date


# In[250]:


now=datetime.now()
date = now.strftime("%Y-%m-%d")
df = pd.DataFrame(columns=['name','description', 'quantity','date','supplier'])
new_row={'name':name, 'description':description , 'quantity': qty, 'date':date,  'supplier':subject_}
df=df.append(new_row, ignore_index=True)

#create a dataframe


# In[251]:



df.to_csv('product.csv',index = True)


# In[ ]:




