from tkinter import *
import smtplib
import time
from tkinter.messagebox import showinfo

root=Tk()

root.title('Submit Order')
# Gets the requested values of the height and width
windowWidth = root.winfo_reqwidth()
windowHeight = root.winfo_reqheight()

# Gets both half the screen width/height and window width/height
positionRight = int(root.winfo_screenwidth()/2 - windowWidth/2)
positionDown = int(root.winfo_screenheight()/3 - windowHeight/2)

def succ_destroy():
    showinfo("Window", "Order Submitted")    
    root.destroy()

def main_screen():
    root.deiconify()
    root.wm_title("Submit Order")
    root.geometry("+{}+{}".format(positionRight-320, positionDown-100))

    header = Frame(root, width=800, height=175)
    header.grid(columnspan=3,rowspan=2,row=0)

    main_content = Frame(root, width=800, height=250)
    main_content.grid(columnspan=3,rowspan=2,row=8)

    global supplier_verify
    global productname_verify
    global desc_verify
    global qty_verify

    #login input
    supplier_verify = StringVar()
    productname_verify = StringVar()
    desc_verify = StringVar()
    qty_verify = StringVar()
    
    Label(header,  text="Supplier Name: ", fg="black", font=("Helvetica", 11)).grid(column=1, row=4)
    lblsupplier = Entry(header, textvariable=supplier_verify, width="30")
    lblsupplier.grid(column=1, row=5,padx=(10, 10), pady=(20, 10))
    Label(header, text="")
    Label(header,  text="Product Name: ", fg="black", font=("Helvetica", 11)).grid(column=1, row=6)
    lblproductname = Entry(header, textvariable=productname_verify, width="30")
    lblproductname.grid(column=1, row=7,padx=(10, 10), pady=(20, 10))
    Label(header, text="")
    Label(header,  text="Description: ", fg="black", font=("Helvetica", 11)).grid(column=1, row=8)
    lbldesc = Entry(header, textvariable=desc_verify, width="30")
    lbldesc.grid(column=1, row=9,padx=(10, 10), pady=(20, 10))
    Label(header, text="")
    Label(header,  text="Quantity: ", fg="black", font=("Helvetica", 11)).grid(column=1, row=10)
    lblqty = Entry(header, textvariable=qty_verify, width="30")
    lblqty.grid(column=1, row=11,padx=(10, 10), pady=(20, 10))

    loginbtn = Button(header, text="Submit", bg="white",width=15, height=1, command=send_email)
    loginbtn.grid(row=12,column=1, padx=(10, 10), pady=(20, 10))

def send_email():
    supplier_name = supplier_verify.get()
    product_name = productname_verify.get()
    product_description = desc_verify.get()
    product_quantity = qty_verify.get()

    mail = smtplib.SMTP('smtp.gmail.com', 587)
    mail.ehlo()
    mail.starttls()
    mail.login('developer.mus@gmail.com','Musdev99' )

    SUBJECT = supplier_name
    BODY = "Product: " + product_name + "\nDescription: " + product_description + "\nQuantity: " + product_quantity

    message = 'Subject: {}\n\n{}' .format(SUBJECT,BODY)
    mail.sendmail('developer.mus@gmail.com','developer.mus@gmail.com',message)
    mail.close
    time.sleep(0.50)
    succ_destroy()


main_screen()
root.resizable(False, False)
root.mainloop()