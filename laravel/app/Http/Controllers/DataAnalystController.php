<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use Illuminate\Support\Facades\DB;

class DataAnalystController extends Controller
{
    public function index(){
        $file = DB::table('files')->get();

        // dd($file);

        return view('dataanalyst.formupload', ['file' => $file]);
    }

    public function upload(Request $request){

        $filename = $request->file('filepdf')->getClientOriginalName();
        $path = $request->file('filepdf')->store('doc');
        
        File::create([

            'filename' => $filename,
            'path' => $path,

        ]);
        
        $item = [];

        if(($open = fopen(storage_path()."/app/".$path,"r")) !== False){

            while (($data = fgetcsv($open, 1000, ",")) !== FALSE) {
                $item[] = $data;
            }

            fclose($open);
        }

        // print_r($item[1][1]);

        return view('/listproduct', ['items' => $item]);

    }
}
