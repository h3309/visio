<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index(){

        // dd(auth()->user()->name);

        // $users = DB::table('roles')
        //     ->join('users', 'roles.id', '=', 'users.role_id')
        //     ->select('*')
        //     ->get();

        // dd($users); 
        return view('dashboard');
    }
}
