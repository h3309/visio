<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Models\File;
use App\Mail\TestMail;


use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class BusinessPartnerController extends Controller
{
    public function index(){
        return view('sendemail');
    }

    public function sendEmail(Request $request){
        
        $path = $request->file('filepdf')->store('doc');

        // dd($path);

        File::create([

            'name' => $path,
            'path' => $path,

        ]);

        $data = array('name'=>"Virat Gandhi");

        Mail::send('mail', $data, function($message) {

           $message->to('amirulaiman6646@gmail.com', 'Tutorials Point')->subject('Laravel Basic Testing Mail');
           $message->from('developer.mus@gmail.com@gmail.com','Virat Gandhi');

        });

        return redirect('/sendemail');
    }

    public function attachment_email() {

        $details = [
            'title' => "testing email",
            'body' => "testing purpose"
        ];

        Mail::to("developer.mus@gmail.com")->send(new TestMail($details));
        return "email Send";


     }
}
