<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function tologin(){
        return view('login');
    }

    public function login(Request $request){

        $credential = $request->validate([           

            'email' => 'required|email:dns',
            'password' => 'required'
        ]);

        if(Auth::attempt($credential)){                  

            $request->session()->regenerate();

            return redirect('/dashboard')->with('success', 'you are now login');
    
        }

        return back()->with('loginError', 'Login Fail');

    }

    public function toregister(){
        return view('register');
    }

    public function register(Request $request){
        $request->validate([

            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required'],
            'name' => ['required']
    
        ]);

        User::create([

            'email' => $request->email,
            'password' => Hash::make($request->password),
            'name' => $request->name,
            'role_id' => 1,

        ]);

        return redirect('login');
    }

    public function logout(){           
        Auth::logout();
    
        request()->session()->invalidate();
    
        request()->session()->regenerateToken();
    
        return redirect('/login');
    
    }
}
