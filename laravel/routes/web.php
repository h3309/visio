<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\BusinessPartnerController;  
use App\Http\Controllers\DataAnalystController;  

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [AuthController::class, 'tologin']);

Route::get('/login', [AuthController::class, 'tologin']);

Route::post('/login', [AuthController::class, 'login']);

Route::get('/register', [AuthController::class, 'toregister']);

Route::post('/register', [AuthController::class, 'register']);

Route::get('/logout', [AuthController::class, 'logout']);

Route::get('/dashboard', [DashboardController::class, 'index']);

Route::get('/tosendemail', [BusinessPartnerController::class, 'index']);

Route::post('/sendEmail', [BusinessPartnerController::class, 'sendEmail']);

Route::get('/touploadfile', [DataAnalystController::class, 'index']);

Route::post('/uploadfile', [DataAnalystController::class, 'upload']);

