@extends('templatefocusadmin.index')
@section('container')
<div class="card">
    <div class="card-title">
        <h4>Update Inventory</h4>
        
    </div>
    
</div>

<div class="card">
    <div class="card-title">
        <h4>All File </h4>
        
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Num</th>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th>quantity</th>
                        <th>Date</th>
                        <th>Supplier</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    

                    @foreach($items as $key => $item)
                    @if($key > 0)
                    <tr>
                        <td scope="row">{{ $i }}</td>
                        <td>{{ $item[1] }}</td>
                        <td>{{ $item[2] }}</td>
                        <td>{{ $item[3] }}</td>
                        <td>{{ $item[4] }}</td>
                        <td>{{ $item[5] }}</td>
                    
                        <?php $i++ ?>
                    </tr>
                    @endif
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>
        
          
@endsection