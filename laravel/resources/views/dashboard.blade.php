@extends('templatefocusadmin.index')
@section('container')
<h2>Welcome {{ auth()->user()->name }}, @can('analyst') Data Analyst @endcan @can('inventory') Inventory Controller @endcan @can('businesspartner') Business Partner @endcan</h2>
@endsection
