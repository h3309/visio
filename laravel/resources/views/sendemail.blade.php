@extends('templatefocusadmin.index')
@section('container')
<div class="card">
    <div class="card-title">
        <h4>Update Inventory</h4>
        
    </div>
    <div class="card-body">
        <div class="basic-form">
            <form action="/sendEmail" method="post" enctype="multipart/form-data">
                @csrf
                <div class="input-group mb-3">
                    <input name="filepdf" type="file" class="form-control" id="inputGroupFile02">
                  </div>
                  <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Submit</button>
            </form>
        </div>
    </div>
</div>
        
          
@endsection