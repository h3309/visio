@include('templatefocusadmin.header')
@include('templatefocusadmin.sidebar')
@include('templatefocusadmin.topbar')
@yield('container');

@include('templatefocusadmin.footer')
