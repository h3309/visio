<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <ul>
                <div class="logo"><a href="index.html">
                        <!-- <img src="assetsfocusadmin/images/logo.png" alt="" /> --><span>Focus</span></a></div>

                @can('analyst')
                <li class="label">Data Analyst</li>
                <li><a href="/touploadfile"><i class="ti-email"></i> Update inventory</a></li>
                @endcan

                @can('inventory')
                <li class="label">Inventory Controll</li>
                <li><a href="app-event-calender.html"><i class="ti-calendar"></i> Calender </a></li>
                <li><a href="app-email.html"><i class="ti-email"></i> Email</a></li>
                <li><a href="app-profile.html"><i class="ti-user"></i> Profile</a></li>
                @endcan

                @can('businesspartner')
                <li class="label">Inventory Controll</li>
                <li><a href="/tosendemail"><i class="ti-email"></i> Update inventory</a></li>
                @endcan


                <li><a href="/logout"><i class="ti-close"></i> Logout</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- /# sidebar -->
