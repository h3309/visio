@extends('templatefocusadmin.index')
@section('container')
<div class="card">
    <div class="card-title">
        <h4>Update Inventory</h4>
        
    </div>
    <div class="card-body">
        <div class="basic-form">
            <form action="/uploadfile" method="post" enctype="multipart/form-data">
                @csrf
                <div class="input-group mb-3">
                    <input name="filepdf" type="file" class="form-control" id="inputGroupFile02">
                  </div>
                  <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Submit</button>
            </form>
        </div>
    </div>
    
</div>

<div class="card">
    <div class="card-title">
        <h4>All File </h4>
        
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th>File Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    @foreach($file as $files)
                    <tr>
                        <th scope="row">{{ $i }}</th>
                        <td>{{ $files->filename }}</td>
                        <td><a href="storage/app/{{ $files->path }}"><button class="btn btn-primary">Get File</button></a></td>
                    </tr>
                    <?php $i++; ?>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
        
          
@endsection